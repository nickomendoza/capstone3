import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

export default function AdminOrders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setOrders(data);
    })
    .catch(error => {
      console.error("Error fetching all orders:", error);
    });
  }, []);

  return (
    <div>
      <h1>All Orders</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>UserID</th>
            <th>OrderID</th>
            <th>Date of Order</th>
            <th>Products</th>
            <th>Total Amount</th>
          </tr>
        </thead>
        <tbody>
          {orders.map(order => (
            <tr key={order._id}>
              <td>{order.userId}</td>
              <td>{order._id}</td>
              <td>{new Date(order.dateTransaction).toLocaleDateString()}</td>
              <td>
                {order.products.map(product => (
                  <p key={product.productId._id}>
                    {product.productId.name} - Quantity: {product.quantity}
                  </p>
                ))}
              </td>
              <td>&#8369; {order.totalAmount}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
