import { useState, useEffect, useContext } from 'react';
import { Container, Card } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Orders() {
    const { user } = useContext(UserContext);
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        if(user.id) {
            fetch(`${process.env.REACT_APP_API_URL}/users/my-orders`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                setOrders(data);
            })
            .catch(error => console.error("Error fetching user orders:", error));
        }
    }, [user.id]);

    return (
        <Container>
            <h1 className='text-center m-5'>YOUR ORDERS</h1>
            {orders.length === 0 ? (
                <p>You haven't placed any orders yet.</p>
            ) : (
                orders.map(order => (
                    <Card key={order._id} className="mb-3">
                        <Card.Header>
                            Order Date: {new Date(order.dateTransaction).toLocaleDateString()}
                        </Card.Header>
                        <Card.Body>
                            {order.products.map(product => (
                                <div key={product.productId._id}>
                                    <Card.Title>{product.productId.name}</Card.Title>
                                    <Card.Text>Price: &#8369;{product.price}</Card.Text>
                                    <Card.Text>Quantity: {product.quantity}</Card.Text>
                                </div>
                            ))}
                            <hr />
                            <h5>Total Amount: &#8369;{order.totalAmount}</h5>
                        </Card.Body>
                    </Card>
                ))
            )}
        </Container>
    );
}
