import { useState, useContext, useEffect } from "react";
import { Form, Button, Modal } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from "sweetalert2";



export default function AddProduct({fetchData}) {

  const { user } = useContext(UserContext);
  const [showModal, setShowModal] = useState(false);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [category, setCategory] = useState('');
  const [imgUrl, setImgUrl] = useState('');
  const [isActive, setIsActive] = useState('');

  const toggleModal = () => {
    setShowModal(!showModal);
  }

  function addNewProduct(e) {
    e.preventDefault();
    
    fetch(`${process.env.REACT_APP_API_URL}/products/add-new-product`, {
      method: 'POST',
      headers: {
        'Content-type' : 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        category: category,
        imgUrl: imgUrl
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      
      if (data) {
        Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'New Product Added!'
        })
        setName('');
        setDescription('');
        setPrice('');
        setCategory('');
        setImgUrl('');
        
        
        toggleModal();
        fetchData();
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }
    })
  }

  useEffect(() => {
		if (name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [name, description, price]);

  return (
    (user.id === null || user.isAdmin === false) ?
      <Navigate to='/products'/>
    :
      <>
        <Button className="fs-4 mb-4 px-4" variant="primary" onClick={toggleModal}>
            Add New Product
        </Button>
        <Modal show={showModal} onHide={toggleModal}>
          <Form onSubmit={(e) => addNewProduct(e)}>
            <Modal.Header closeButton>
                <Modal.Title className="">Add New Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
			            <Form.Group className='m-3'>
			                <Form.Label>Name:</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	placeholder="Enter Name" 
			                	value ={name}
			                	onChange={e => {setName(e.target.value)}}
			                />
			            </Form.Group>

			            <Form.Group className='m-3'>
			                <Form.Label>Description</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter Description" 
				                required
				                value ={description}
			                	onChange={e => {setDescription(e.target.value)}}
			                />
			            </Form.Group>

			            <Form.Group className='m-3'>
			                <Form.Label>Price:</Form.Label>
			                <Form.Control 
				                type="number" 
				                placeholder="Enter Price" 
				                required
				                value ={price}
			                	onChange={e => {setPrice(e.target.value)}}
			               	/>
			            </Form.Group>
                  <Form.Group className='m-3'>
			                <Form.Label>Category:</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	placeholder="Enter Category" 
			                	value ={category}
			                	onChange={e => {setCategory(e.target.value)}}
			                />
			            </Form.Group>
                  <Form.Group className='m-3'>
			                <Form.Label>Image Url:</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	placeholder="Enter url" 
			                	value ={imgUrl}
			                	onChange={e => {setImgUrl(e.target.value)}}
			                />
			            </Form.Group>
            </Modal.Body>
            <Modal.Footer>
              <Button className='me-1' variant="primary" type="submit" disabled = {isActive === false}>Submit</Button>
              <Button className='ms-3' variant="secondary" onClick={toggleModal}>Close</Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </>

  )
}