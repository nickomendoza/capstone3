import { Card, Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect,useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import { ReactComponent as Logo} from '../img/Asset2.svg';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register () {

  const {user} = useContext(UserContext);
	const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNum, setMobileNum] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


  const registerUser = (e) =>{
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNum: mobileNum,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data) {
        setFirstName('')
        setLastName('')
        setEmail('')
        setMobileNum('')
        setPassword('')
        setConfirmPassword('')

        Swal.fire({
					title: 'New User Created!',
					icon: 'success',
					text: 'You can now log in your Account!'

				})
				navigate('/login');
      } else {
        Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again.'

				})
      }

    })

  }

  useEffect(() =>{

    if (firstName !== '' && lastName !== '' && email !== '' && mobileNum !== '' && password !== '' && confirmPassword !== ''){
      setIsActive(true);
    }else{
      setIsActive(false);
    }

  }, [firstName, lastName, email, mobileNum, password, confirmPassword])

  return (
    (user.id !== null) ?
		    <Navigate to="/products" />
				:
				<Container>
					<Row className="d-flex justify-content-center align-items-center min-vh-100">
						<Col>
							<Logo/>
						</Col>
						<Col>
							<Card className="mx-auto my-5" style={{ width: '30rem' }}>
								<Card.Header><h1 className='mx-2 p-2'>Register</h1></Card.Header>
								<Card.Body>
										<Form onSubmit={(event) => registerUser(event)}>
														<Form.Group className='m-3'>
																<Form.Label className='fw-bolder fs-5'>First Name:</Form.Label>
																<Form.Control 
																	type="text" 
																	placeholder="Enter First Name" 
																	value ={firstName}
																	onChange={event => {setFirstName(event.target.value)}}
																/>
														</Form.Group>
														<Form.Group className='m-3'>
																<Form.Label className='fw-bolder fs-5'>Last Name:</Form.Label>
																<Form.Control 
																	type="text" 
																	placeholder="Enter Last Name" 
																	required
																	value ={lastName}
																	onChange={event => {setLastName(event.target.value)}}
																/>
														</Form.Group>
														<Form.Group className='m-3'>
																<Form.Label className='fw-bolder fs-5'>Email:</Form.Label>
																<Form.Control 
																	type="email" 
																	placeholder="Enter Email" 
																	required
																	value ={email}
																	onChange={event => {setEmail(event.target.value)}}
																/>
														</Form.Group>
														<Form.Group className='m-3'>
																<Form.Label className='fw-bolder fs-5'>Mobile No:</Form.Label>
																<Form.Control 
																	type="number" 
																	placeholder="Enter 11 Digit No." 
																	required
																	value ={mobileNum}
																	onChange={event => {setMobileNum(event.target.value)}}
																/>
														</Form.Group>
														<Form.Group className='m-3'>
																<Form.Label className='fw-bolder fs-5'>Password:</Form.Label>
																<Form.Control 
																	type="password" 
																	placeholder="Enter Password" 
																	required
																	value ={password}
																	onChange={event => {setPassword(event.target.value)}}
																/>
														</Form.Group>
														<Form.Group className='m-3'>
																<Form.Label className='fw-bolder fs-5'>Confirm Password:</Form.Label>
																<Form.Control 
																	type="password" 
																	placeholder="Confirm Password" 
																	required
																	value ={confirmPassword}
																	onChange={event => {setConfirmPassword(event.target.value)}}
																/>
														</Form.Group>

														<Button className='m-3 fs-5' variant="primary" type="submit" disabled = {isActive === false}>Submit</Button>

														
										</Form>
								</Card.Body>
								<Card.Footer><p className='text-center my-auto'>Already Have an Account? <Link to="/login">Log in now!</Link></p></Card.Footer>
							</Card>
						</Col>
					</Row>

				</Container>
				
  )
}