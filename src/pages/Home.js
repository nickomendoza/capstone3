import Banner from '../components/Banner.js';



export default function Home(){
	return(
		<>
			<Banner 
				title="PC - HUB" 
				subtitle = "All components matter!"
				buttonText ="Shop now!"
				buttonLink ="/login"
			/>
		</>
	)
}