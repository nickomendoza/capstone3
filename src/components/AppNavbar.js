import { useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	// // const [user, setUser] = useState(localStorage.getItem("token"))

	const { user } = useContext(UserContext);
	// console.log(user);

	return (
		<Container fluid className='bg-info'>
			<Navbar expand="lg" >
				<Container>
						<Navbar.Brand className='text-light fw-bold fs-4' as={Link} to="/">PC HUB</Navbar.Brand>
						<Navbar.Toggle aria-controls="basic-navbar-nav"/>
						<Navbar.Collapse id="basic-navbar-nav">
							<Nav className="ms-auto	fs-5">
								<Nav.Link className="text-light" as={NavLink} to="/">HOME</Nav.Link>
								
								{(user.isAdmin === true) ?
								<>
								<Nav.Link className="text-light" as={NavLink} to="/orders">ORDERS</Nav.Link>
									<Nav.Link className="text-light" as={NavLink} to="/products">Admin Dashboard</Nav.Link>
								</>
									:
									<Nav.Link className="text-light" as={NavLink} to="/products">Products</Nav.Link>
								}

								{(user.id !== null) ?
										user.isAdmin ?
												<Nav.Link className="text-light" as={NavLink} to="/logout">Logout</Nav.Link>
											:
											<>
												<NavDropdown title={<span><svg width="40" height="30" fill="#ffffff" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																	<path d="M12 2.25c-5.376 0-9.75 4.374-9.75 9.75s4.374 9.75 9.75 9.75 9.75-4.374 9.75-9.75S17.376 2.25 12 2.25ZM9.646 7.726c.594-.63 1.43-.976 2.354-.976.924 0 1.753.349 2.349.982.604.64.898 1.502.829 2.429C15.038 12 13.614 13.5 12 13.5c-1.614 0-3.042-1.5-3.178-3.34-.069-.934.225-1.798.824-2.434ZM12 20.25a8.227 8.227 0 0 1-5.906-2.495c.44-.626 1-1.16 1.647-1.567C8.936 15.422 10.448 15 12 15c1.552 0 3.064.422 4.258 1.188a5.76 5.76 0 0 1 1.648 1.567A8.223 8.223 0 0 1 12 20.25Z"></path>
																	</svg></span>} 
															id="basic-nav-dropdown">
														<NavDropdown.Item as={NavLink} to="/cart">My Cart</NavDropdown.Item>
														<NavDropdown.Item as={NavLink} to="/orders">My Orders</NavDropdown.Item>
														<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
												</NavDropdown>
											</>
									:
										<>
											<Nav.Link className="text-light" as={NavLink} to="/login">Login</Nav.Link>
											<Nav.Link className="text-light" as={NavLink} to="/register">Register</Nav.Link>
										</>
								}
							</Nav>
						</Navbar.Collapse>
				</Container>
			</Navbar>
		</Container>
	)
}