import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct ({productId, inStock, fetchData}) {
  const archiveToggle = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        inStock: inStock
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      if (data === true) {
        Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product archived successfully.'
				})
        fetchData()
      } else {
        Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again.'
				})
      }
    })
  }

  const activateToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product activated successfully.'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

  return (

    <>
			{ (inStock === true) ?
				<Button variant="danger" onClick={archiveToggle}>Archive</Button>
				:
				<Button variant="success" onClick={activateToggle}>Activate</Button>
			}

		</>

  )
}