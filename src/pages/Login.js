import { useState, useEffect, useContext } from 'react';
import { Container, Row , Col, Card, Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import { ReactComponent as Logo} from '../img/Asset 1.svg';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function Login (){
  
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);
  

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(result => result.json())
    .then(data => {
      console.log(data.access)

      if (data.access) {
        localStorage.setItem('token', data.access)

        retrieveUserDetails(data.access);
          Swal.fire({
            title: "Login successful!",
            icon: "success",
            text: "Happy Shopping!!"
          }) 
      } else {
        Swal.fire({
          title: "Authentication failed.",
          icon: "error",
          text: "Check your login details and try again."
        })
      }
    })

    setEmail('');
    setPassword('');
  };


  const retrieveUserDetails = (token) => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization : `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }

  useEffect(() => {
    if(email !== '' && password !== ''){
      setIsActive(true);
    }else{
      setIsActive(false);
    }
  }, [email, password]);
  return (
            (user.id !== null) ?
                <Navigate to="/products" />
            :
              <Container>
                <Row className="d-flex justify-content-center align-items-center min-vh-100">
                  <Col>
                    <Logo />
                  </Col>
                  <Col>
                    <Card className="mx-auto my-5 text-center" style={{ width: '30rem' }}>
                      <Card.Header><h1 className='mx-2 p-2'>Login</h1></Card.Header>
                      <Card.Body>
                          <Form onSubmit={(e) => authenticate(e)} >
                              <Form.Group className='m-3' controlId="userEmail">
                                  <Form.Label className='fw-bolder fs-4'>Email address</Form.Label>
                                  <Form.Control className='text-center'
                                      type="email" 
                                      placeholder="Enter email"
                                      value={email}
                                      onChange={(e) => setEmail(e.target.value)}
                                      required
                                  />
                              </Form.Group>

                              <Form.Group controlId="password" className="m-3">
                                  <Form.Label className='fw-bolder fs-4'>Password</Form.Label>
                                  <Form.Control className='text-center'
                                      type="password" 
                                      placeholder="Password"
                                      value={password}
                                      onChange={(e) => setPassword(e.target.value)}
                                      required
                                  />
                              </Form.Group>
                              { isActive ? 
                                  <Button variant="primary" type="submit" id="submitBtn" className="m-3 fs-5">
                                      Submit
                                  </Button>
                                  : 
                                  <Button variant="info" type="submit" id="submitBtn" disabled className="m-3 fs-5">
                                      Submit
                                  </Button>
                              }
                          </Form>
                      </Card.Body>
                      <Card.Footer><p className='text-center my-2'>New User? <Link to="/register">Create your Account now!</Link></p></Card.Footer>
                    </Card>
                  </Col>
                  
                </Row>
              </Container>
  )
}