import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import ArchiveProduct from './ArchiveProduct';
import EditProduct from './EditProduct';
import AddProduct from './AddProduct';

export default function AdminView({productData, fetchData}) {

    const [products, setProducts] = useState([])

    useEffect(() => {
        const productArr = productData.map(product => {
            return (
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>{product.price}</td>
                    <td className={product.inStock ? "text-success" : "text-danger"}>
                        {product.inStock ? "Available" : "Unavailable"}
                    </td>
                    <td><EditProduct product={product._id} fetchData={fetchData} /></td>        
                    <td><ArchiveProduct productId={product._id} inStock={product.inStock} fetchData={fetchData}/></td>
                    
                </tr>
            )
        })

        setProducts(productArr)
    }, [productData, fetchData])

    return(
        <>
            <h1 className="text-center my-4">ADMIN DASHBOARD</h1>

            <Table striped bordered hover responsive variant='dark'>
                <thead>
                    <tr className="text-center text-secondary">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>
            </Table>
            <div>
                <AddProduct fetchData={fetchData}/>
            </div>
            
        </>

        )
}
