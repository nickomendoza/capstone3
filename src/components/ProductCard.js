import { Col, Card} from 'react-bootstrap';
// import { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {
  const {name, description, price, category, imgUrl, inStock, _id} = productProp;
  // const [showFullDescription, setShowFullDescription] = useState(false);

  return (
    <Col lg={3} md={6} sm={12} className='my-3'>
      <Card className='cardProduct'>
        <Card.Img className='p-4' variant="top" src={imgUrl} />
        <Card.Body className='p-3'>
          <Card.Title className='fs-4 my-2'><Link className='rm_decoration text-dark' to={`/products/${_id}`}>{name}</Link></Card.Title>
          
          {/* <Card.Text>
            {showFullDescription ? description : `${description.slice(0, 100)}`}
            {description.length > 100 && !showFullDescription && (
              <span
                className="text-primary cursor-pointer"
                onClick={() => setShowFullDescription(true)}
              >
                 ..See More
              </span>
            )}
          </Card.Text> */}
          {/* <Card.Subtitle>Price:</Card.Subtitle> */}
          <Card.Subtitle className='fw-bold my-2'>&#8369; {price}</Card.Subtitle>
          <Card.Text className='fst-italic fw-light mt-2'>{category}</Card.Text>
        </Card.Body>
        <Card.Footer className='d-flex justify-content-between align-items-center my'>
          <Card.Text className='fst-italic fw-bold stock-color'>{inStock ? 'In Stock' : null}</Card.Text>
          <Link className="btn btn-info fw-bold text-light" to={`/products/${_id}`}>BUY NOW</Link>
        </Card.Footer>
      </Card>
    </Col>
  );
}
ProductCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		// description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
    category: PropTypes.string.isRequired
	})
}