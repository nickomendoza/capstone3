import {useContext} from 'react';
import UserContext from '../UserContext';
import AdminOrders from '../components/AdminOrder';
import UserOrder from '../components/UserOrder'




export default function Product() {
  const {user} = useContext(UserContext)

  return (
    <>
			{
				(user.isAdmin === true) ?
					<AdminOrders />
					:
					<UserOrder/>
			}
		</>

  
  )
}