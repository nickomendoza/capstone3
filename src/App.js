import './App.css';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext.js';
import AppNavbar from './components/AppNavbar.js';
import Product from './pages/Product';
import { useState, useEffect } from 'react';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Error from './pages/Error';
import Home from './pages/Home';
import ProductView from './pages/ProductView';
import CartPage from './pages/Cart';
import Order from './pages/Order';



export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details/`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        }else{
          setUser ({
            id: null,
            isAdmin: null
          });
        }
    })
  }, []);


  return (
    <UserProvider value = {{user, setUser, unsetUser}} >
      <Router>
       <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element= {<Home/>} />
            <Route path="/products" element= {<Product/>} />
            <Route path="/cart" element= {<CartPage/>} />
            <Route path="/products/:productId" element= {<ProductView/>} />
            <Route path="/orders" element= {<Order/>} />
            <Route path="/register" element= {<Register/>} />
            <Route path="/login" element= {<Login/>} />
            <Route path="/logout" element= {<Logout/>} />
            <Route path="*" element= {<Error/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  )
}