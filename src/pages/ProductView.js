import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [category, setCategory] = useState('');
  const [imgUrl, setImgUrl] = useState('');
  const [quantity, setQuantity] = useState(1);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setCategory(data.category);
        setImgUrl(data.imgUrl);
      });
  }, [productId]);

  const handleIncrement = () => {
    setQuantity(quantity + 1);
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const handleAddToCart = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/cart/add-to-cart`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          productId: productId,
          quantity: quantity
        })
      });

      const data = await response.json();
      if (response.ok) {
        Swal.fire({
          title: "Product Added to Cart!",
          icon: "success",
          text: "Happy Shopping!!"
        });

        navigate('/products');
      } else {
        Swal.fire({
          title: "Something went wrong.",
          icon: "error",
          text: "Please try again."
        });
      }
    } catch (error) {
      console.error("Error adding product to cart:", error);
      alert("Failed to add product to cart. Please try again.");
    }
  };

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }} className='my-4'>
          <Card className='p-4'>
            <Card.Img variant="top" src={imgUrl} className='p-4' />
            <Card.Body>
              <Card.Title className='pb-2 fs-3'>{name}</Card.Title>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text className='fw-bold'>&#8369;{price}</Card.Text>
              <Card.Text className='fst-italic'>{category}</Card.Text>
              {user && user.id !== null ? (
                <div>
                  <div className="d-flex align-items-center mb-3">
                    <button className="btn btn-info text-light me-2" onClick={handleDecrement}>-</button>
                    <input type="number" value={quantity} readOnly className="form-control w-25 text-center" />
                    <button className="btn btn-info text-light ms-2" onClick={handleIncrement}>+</button>
                  </div>
                  <Button className="btn btn-info fw-bold text-light" variant="info" onClick={handleAddToCart}>
                    Add to Cart
                  </Button>
                </div>
              ) : (
                <Link className="btn btn-info fw-bold text-light" to="/login">
                  BUY NOW
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
