import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'; 

export default function Cart() {
    const { user } = useContext(UserContext);
    const [cartItems, setCartItems] = useState([]);

    useEffect(() => {
        if (user.id) {
            fetch(`${process.env.REACT_APP_API_URL}/cart`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                if (data && data.addedProducts) {
                    setCartItems(data.addedProducts);
                }
            })
            .catch(error => console.error("Error fetching user cart:", error));
        }
    }, [user.id]);

    const calculateTotalPrice = () => {
        return cartItems.reduce((total, item) => total + (item.productId.price * item.quantity), 0);
    };

    const removeItem = async (productId) => {
      Swal.fire({
          title: 'Are you sure?',
          text: 'You are about to remove this item from your cart.',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, remove it!',
          cancelButtonText: 'Cancel'
      }).then(async (result) => {
          if (result.isConfirmed) {
              try {
                  await fetch(`${process.env.REACT_APP_API_URL}/cart/item/${productId}`, {
                      method: 'DELETE',
                      headers: {
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }
                  });
                  
                  setCartItems(prevCartItems => prevCartItems.filter(item => item.productId._id !== productId));

                  Swal.fire('Removed!', 'The item has been removed from your cart.', 'success');
              } catch (error) {
                  console.error("Error removing item from cart:", error);
              }
          }
      });
    };

    const handleCheckout = async () => {
      try {
          const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
              method: 'POST',
              headers: {
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
          });
  
          if (response.ok) {
              Swal.fire('Success!', 'Your order has been placed.', 'success');
              setCartItems([]);
          } else {
              Swal.fire('Error!', 'There was an error placing your order.', 'error');
          }
      } catch (error) {
          console.error('Error placing order:', error);
      }
    };


    return (
        <div>
            <h1 className='text-center m-5'>YOUR CART</h1>
            {cartItems.length === 0 ? (
                <p>Your cart is empty.</p>
            ) : (
                <Container>
                    <Row className=''>
                        <Col className='my-4'>
                            {cartItems.map(item => (
                                <Card key={item.productId._id} className="mb-3">
                                    <Card.Body>
                                        <Card.Title>{item.productId.name}</Card.Title>
                                        <Card.Text>Price: &#8369;{item.productId.price}</Card.Text>
                                        <Card.Text>Quantity: {item.quantity}</Card.Text>
                                        <Card.Text>Total Price: &#8369;{item.productId.price * item.quantity}</Card.Text>
                                        <Button variant="danger" onClick={() => removeItem(item.productId._id)}>
                                            Remove
                                        </Button>
                                    </Card.Body>
                                </Card>
                            ))}
                        </Col>
                    </Row>
                    <Row className='justify-content-end'>
                        <Col md={4} className='mt-2 mb-4'>
                            <h4>Total Price: &#8369;{calculateTotalPrice()}</h4>
                            <Button variant='info text-light fs-4 fw-bold' onClick={handleCheckout}>
                              Checkout
                            </Button>
                        </Col>
                    </Row>
                </Container>
            )}
        </div>
    );
}
