import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function EditProduct({product, fetchData}) {

  const [productId, setProductId] = useState('');
  
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	const [imgUrl, setImgUrl] = useState('');
  const [showEdit, setShowEdit] = useState('');

  const openModal = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then(res => res.json())
    .then(data => {
      setProductId(data._id);
      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
			setCategory(data.category);
			setImgUrl(data.imgUrl);
    })

    setShowEdit(true);

  }

  const closeModal = () => {
    setShowEdit(false);
    setName('');
    setDescription('');
    setPrice(0);
		setCategory('');
		setImgUrl('');
  }

  const editProduct = (e, productId) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/edit-info`, {
      method: 'PUT',
      headers: {
        'Content-Type' : 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
				category: category,
				imgUrl: imgUrl
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      if (data === true){
        Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product updated successfully.'
				})
				closeModal();
				fetchData();
      } else {
        Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again.'
				})
				closeModal();
        }
    })
  }

  return (
    <>
			<Button variant="primary" onClick={() => openModal(product)}>Edit</Button>

			<Modal show={showEdit} onHide={closeModal}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								value={name}
								onChange={e => setName(e.target.value)} 
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								value={description}
								onChange={e => setDescription(e.target.value)} 
								required
							/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group controlId="productCategory">
							<Form.Label>Category</Form.Label>
							<Form.Control 
								type="text" 
								value={category}
								onChange={e => setCategory(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group controlId="productImgUrl">
							<Form.Label>Image Url</Form.Label>
							<Form.Control 
								type="text" 
								value={imgUrl}
								onChange={e => setImgUrl(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeModal}>Close</Button>
						<Button variant="primary" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>

  )
}