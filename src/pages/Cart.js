import { useContext } from 'react';
import UserContext from '../UserContext';
import Cart from '../components/CartComponent';

export default function CartPage() {
  const { user } = useContext(UserContext);

  return (
    <div>
      {user.id ? (
        <Cart />
      ) : (
        <p>Please log in to view your cart.</p>
      )}
    </div>
  );
}
