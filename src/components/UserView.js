import { useState, useEffect } from 'react';
import {Row} from 'react-bootstrap';
import ProductCard from './ProductCard';
// import CourseSearch from './CourseSearch.js';
// import CourseSearchByPrice from './CourseSearchByPrice.js'

export default function UserView({productData}) {

	const [products, setProduct] = useState([]);

	useEffect(() => {
		const productArr = productData.map(product => {
			if(product.inStock === true) {
				return (
					<ProductCard productProp={product} key={product._id} />
				)
			} else {
				return null;
			}
		})

		setProduct(productArr);
	}, [productData])


	return (

		<>
			<h1 className="text-center my-5">Products</h1>
			{/* <CourseSearch />
			<CourseSearchByPrice /> */}
			<Row>{ products }</Row>
		</>
	)

}
